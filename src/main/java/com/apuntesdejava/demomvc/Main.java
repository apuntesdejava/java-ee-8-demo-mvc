package com.apuntesdejava.demomvc;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("main-app")
public class Main extends Application{
    
}
