package com.apuntesdejava.demomvc.controller;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.mvc.Controller;
import javax.mvc.Models;
import javax.mvc.View;
import javax.mvc.Viewable;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/saludo")
@Controller
@RequestScoped
public class HolaController {

    private static final Logger LOG = Logger.getLogger(HolaController.class.getName());

    @Inject
    Models models;

    @GET
    public String hola() {
        return "/WEB-INF/jsp/saludo.jsp";
    }

    @GET
    @View("/WEB-INF/jsp/saludo.jsp")
    @Path("/void")
    public void metodoVoid() {
        LOG.info("Método de tipo void");
    }

    @GET
    @Path("/viewable")
    public Viewable metodoViewable() {
        LOG.info("Método de tipo Viewable");
        return new Viewable("/WEB-INF/jsp/saludo.jsp");
    }

    @GET
    @Path("/response")
    public Response metodoResponse() {
        LOG.info("Método de tipo Response. Bien JAX-RS");
        return Response.status(Response.Status.OK)
                .entity("/WEB-INF/jsp/saludo.jsp")
                .build();
    }

    @POST
    @Path("/nombre")
    public String holaNombre(@FormParam("nombre") String nombre) {
        String mensaje = "Hola " + nombre;
        models.put("mensaje", mensaje);
        return "/WEB-INF/jsp/saludo-nombre.jsp";

    }

    @GET
    @Path("/lista-nombres")
    @View("/WEB-INF/jsp/lista-nombres.jsp")
    public void listaNombres() {
        List<String> nombres = Arrays.asList("Ann", "Bernard", "Carl");
        models.put("nombres", nombres);
    }
}
