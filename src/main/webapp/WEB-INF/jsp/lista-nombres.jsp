<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Lista de nombres</h1>
          <%= request.getAttribute("nombres")  %>
        <ul>
            <c:forEach items="${nombres}" var="n">
                <li>${n}</li>
            </c:forEach>
        </ul>
    </body>
</html>
